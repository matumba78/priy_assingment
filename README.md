# Introduction
This project contains assignments on Django & Python Basics.It gives basic ideas of api and database conectivity .
The main goal of this project is to learn basic Django and implement it.Request has been implemented such as GET,POST
PUT,DELETE.
This project basically use SQLite3 Database .
There are different classes and methods which are implemented for deletion,  creation, fetching information, updation.
      

##Running of Code
To run the code there are some  prerequisite 
            1-Python 2.7
            2-Django 1.8
            3-postman
            
To run the code just open terminal and go to the directory where the code folder is there . 
Runserver by using command [python manage.py runserver]
To run operation there are methods used to request such as get, post, put ,delete.
Now there are urls provided for certain operations such as :
Mathematical operation : http://127.0.0.1:8000/test/sum/?p1=4&p2=4 [p1 & p2 are arguments]
Fetch entries from database : http://127.0.0.1:8000/test/update/
Create entries :http://127.0.0.1:8000/test/create                                   
Delete Entries : http://127.0.0.1:8000/test/delete/id.              [id is the id no in the database ]
Update Record : http://127.0.0.1:8000/test/change/id                [id is the id no in the database]

Create records, delete records and update records will be done using postman.
        
            
    




